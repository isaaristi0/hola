import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class AppComponent {
  title = 'appAngular';
  items: Items[];
  primera: boolean;
  segunda: boolean;
  tercera: boolean;

  imagen: String;
  placaReal: String;
  placaOCR: String;
  placaAlgoritmo: String;

  fileData: File = null;
  constructor(private http: HttpClient, private ref: ChangeDetectorRef) { }

  displayedColumns: string[] = ['imagen', 'placa', 'placaC'];
  dataSource = ELEMENT_DATA;

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  test() {
    console.log('test');
  }

  cellClicked(element) {
    this.imagen = element.imagen;
    this.placaReal = element.placa;
    this.placaAlgoritmo = element.placa;
    console.log(element.placa + ' cell clicked');
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.fileData);
    this.http.post('url/to/your/api', formData)
      .subscribe(res => {
        console.log(res);
        alert('SUCCESS !!');
      })
  }

  ngOnInit() {
    this.primera = true;
    this.segunda = false;
    this.tercera = false;
  }

  showPrimera() {
    this.primera = true;
    this.segunda = false;
    this.tercera = false;
  }

  showComparacion() {
    this.primera = false;
    this.segunda = false;
    this.tercera = true;
  }

  mostrarSiguiente() {
    this.primera = false;
    this.segunda = true;
    this.tercera = false;
    this.items = [];

    /*let n1: Items = new Items();
    n1.imagen = "../"+path;
    debugger;
    n1.placa = "ABC123"; */

    let n1: Items = new Items();
    n1.imagen = "../assets/apartment-bed-bedroom-2082087.jpg";
    n1.placa = "ABC123";

    let n2: Items = new Items();
    n2.imagen = "../assets/apartment-bed-bedroom-2082087.jpg";
    n2.placa = "DEF456";

    let n3: Items = new Items();
    n3.imagen = "../assets/apartment-bed-bedroom-2082087.jpg";
    n3.placa = "GHI789";

    this.items.push(n1);
    this.items.push(n2);
    this.items.push(n3);
    this.ref.detectChanges();
  }

  filesPicked(files) {
    var path = "";
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      console.log(file);
      path = file.webkitRelativePath;
      // upload file using path
    }
    this.mostrarSiguiente();
  }

}

export class Items {
  imagen: string;
  placa: string;
}

const ELEMENT_DATA: Items[] = [
  {imagen: "../assets/apartment-bed-bedroom-2082087.jpg", placa: 'ABC123'},
  {imagen: "../assets/apartment-bed-bedroom-2082087.jpg", placa: 'DEF123'},
  {imagen: "../assets/apartment-bed-bedroom-2082087.jpg", placa: 'GHI123'},
  {imagen: "../assets/apartment-bed-bedroom-2082087.jpg", placa: 'JKL123'},
  {imagen: "../assets/apartment-bed-bedroom-2082087.jpg", placa: 'MNO123'},
  {imagen: "../assets/apartment-bed-bedroom-2082087.jpg", placa: 'PQR123'},
  {imagen: "../assets/apartment-bed-bedroom-2082087.jpg", placa: 'STU123'},
];